<?php
$isFound = false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = $_POST["title"];
    $description = $_POST["description"];
    $catetories = $_POST["catetories"];


    echo '$title: ' . $title;
    echo '$description: ' . $description;
    echo '$catetories' . implode(" ", $catetories);

    $image = $_FILES["foodimage"]["tmp_name"];

    if (move_uploaded_file($image, "../upload/" . $_FILES["foodimage"]["name"])) {
        echo 'uploaded';
    } else {
        echo 'failed to upload';
    }
}
?>

<?php include '../partials/header.php'; ?>

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Create new food</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="createFood.php" method="post" enctype="multipart/form-data" role="form">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" value="" class="form-control" />
                                <p class="help-block">Example block-level help text here.</p>
                            </div>

                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" cols="5" rows="5" name="description"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Categories</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="catetories[]" value="cat1">Checkbox 1
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="catetories[]" value="cat2">Checkbox 2
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="catetories[]" value="cat3">Checkbox 3
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Food Image</label>
                                <input class="form-control" type="file" name="foodimage"/>
                            </div>

                            <button type="submit" class="btn btn-default">Create</button>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->

<?php include '../partials/footer.php'; ?>

