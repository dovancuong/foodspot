<?php include '../partials/login_signup_header.php'; ?>

<?php
    // Create connection
    $conn = connectToDB();

    // Check connection
    if ($conn->connect_error) {
        
        echo "Connection failed: " . $conn->connect_error;
    } else {
        echo "Connected successfully";
    }
?>

<?php
    $users = array(
        array("username" => "user1", "password" =>"pass1"),
        array("username" => "user2", "password" => "pass2"),
        array("username" => "user3", "password" => "pass3"),
        ); 
    $isFound = false;
    
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = $_POST["username"];
        $pass = $_POST["password"];        
        foreach($users as $user) {
            $dbemail = $user["email"];
            $dbpassword = $user["password"];
            
            if($username == $dbemail && $pass == $dbpassword) {
                $isFound = true;
                break;
            } else {
                $isFound = false;   
            }
        }

        
    }

            
?>


                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign Up</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Confirm Your Password" name="confirmpassword" type="password" value="">
                                </div>
                                
                                <!-- Change this to a button or input when using this as a form -->
                                <a href="index.html" class="btn btn-lg btn-success btn-block">Sign up</a>
                            </fieldset>
                        </form>
                    </div>
                </div>


<?php include '../partials/login_signup_footer.php'; ?>
