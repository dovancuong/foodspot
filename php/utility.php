<?php
    session_start();
    
    function connectToDB() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "FoodSpot";
        
        // Create connection
        return $conn = new mysqli($servername, $username, $password, $dbname);
    }
    
    function isUserLogined() {
        $cookie_name = "loginEmail";
        
        if(isset($_COOKIE[$cookie_name]) || isset($_SESSION[$cookie_name]))
            return true;
        else
            return false;
    }
    
    function signout() {
        $cookie_name = "loginEmail";
        
        setcookie($cookie_name, "", time() - 3600, "/");
        
        $_SESSION[$cookie_name] = null;
    }

    function parse($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    
    function containOnlyAlphabet($data) {
        return preg_match("/^[a-zA-Z ]*$/", $data);
    }
    
    function isEmailFormat($data) {
        return filter_var($data, FILTER_VALIDATE_EMAIL);
    }
    
    function isURLFormat($data) {
        return filter_var($data, FILTER_VALIDATE_URL);
    }
?>
